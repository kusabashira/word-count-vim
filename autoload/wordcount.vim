" count number of word in buffer
" Version: 0.0.1
" Author: Kusabashira <kusabashira227@gmail.com>
" License: MIT LICENSE

let s:save_cpo = &cpo
set cpo&vim

function! wordcount#count()
	let noChar = 0
	for s:line in getline(0, '$')
		let noChar += strchars(s:line)
	endfor
	return string(noChar)
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
